import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap, tap } from 'rxjs/operators';
import { CountriesService } from '../../services/countries.service';
import { Country } from '../../interfaces/countries.interfaces';
import { of, pipe } from 'rxjs';

@Component({
  selector: 'app-selector-page',
  templateUrl: './selector-page.component.html',
  styles: [
  ]
})
export class SelectorPageComponent implements OnInit {
  // array for selectors 
  regions: string[]      = [];
  countries: Country[]   = [];

  bordersArray: any = [] || null;
  bordersByCode: any = [];
  newBorderArray: string[] = [];
  loading: boolean = false;
  
  myformSelector: FormGroup = this.formGroup.group({
    region: ['', Validators.required ],
    country: ['', Validators.required ],
    bordersCountry: ['', Validators.required ],
  });

  constructor( 
    private formGroup:FormBuilder,
    private countriesServices:CountriesService 
  ) { }

  ngOnInit(): void {

    this.regions = this.countriesServices.regionsData;
    this.myformSelector.get('region')?.valueChanges
    .pipe(
      tap( ( ) => {
        this.myformSelector.get('country')?.reset('');
        this.loading = true;
      }),   
      switchMap( region => this.countriesServices.getRegionByCountry( region ) )
    )
    .subscribe(
      countriesResponse => {
        this.countries = countriesResponse;
        this.loading = false;
      }
    )
    this.myformSelector.get('country')?.valueChanges
    .pipe(
      tap( ( ) => {
        this.bordersByCode = [];
        this.myformSelector.get('bordersCountry')?.reset('');
        this.loading = true;
      }),
      switchMap( countryCode => this.countriesServices.getBordersByCountry(countryCode) )
    )
    .subscribe( countriesReponse => {
        
        if(countriesReponse){
          console.log(countriesReponse);
          this.bordersArray = countriesReponse;
          this.newBorderArray = [...this.bordersArray[0].borders];
          this.countriesServices.getBordersOfCountryByCode( this.newBorderArray ).subscribe(
            countryBordersResponse => {
              this.bordersByCode = countryBordersResponse;
              this.loading = false;
            }
          )
        }
      }
    )
  }

  saveCountryStepOne() {
    console.log( this.myformSelector.value );
  }

}
