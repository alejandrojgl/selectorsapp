import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { Country, CountryCode } from '../interfaces/countries.interfaces';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  private baseUrl: string =  "https://restcountries.eu/rest/v2";
  private regions: string[] = ['Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];

  get regionsData(): string[]  {
    return [...this.regions];
  }

  constructor( private http: HttpClient ) { }

  getRegionByCountry( region: string ): Observable<Country[]> {
    const url: string = `${this.baseUrl}/region/${region}?fields=alpha3Code;name`;
    return this.http.get<Country[]>( url );
  }

  getBordersByCountry( code : string ): Observable<CountryCode | null> {
    if (!code) {
      return of(null)
    }
    const url: string = `${this.baseUrl}/alpha?codes=${code}`;
    return this.http.get<CountryCode>( url );
  }

  getBordersOfCountryByCode(borders:string[]): Observable<Country> {
    const newBorders = borders.join(';');
    console.log(newBorders);
    const url: string = `${this.baseUrl}/alpha?codes=${newBorders}&fields=alpha3Code;name`;
    return this.http.get<Country>( url );
  }
}
